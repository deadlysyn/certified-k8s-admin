# Certified Kubernetes Administrator

- https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests
- https://kubernetes.io/docs/reference/kubectl/cheatsheet

The Udemy course linked above is a great overview, but aging fast (a year old as of writing).
Look for an updated version or additional courses if planning for the CKA, especially with
Kubernetes 1.16 being released.

## Gitops

Following the UNIX philosophy, there is often more than one way to solve a problem
in Kubernetes. This provides flexibility. For example, you can roll out a new deployment
delcaratively or imperatively:

```bash
# delcarative
vi deployment.yml # update image version
git commit -m 'bump image version' deployment.yml
kubectl apply -f deployment.yml

# imperative
kubectl set image deployment/yourapp image=image:1.1
```

It's good to know what's possible (useful in the lab), but notes below assume a
preference for using Git as a source of truth, and using delcarative configuration
to drive changes. This is the heart of Gitops, and avoids getting into a state where
a future `kubectl apply` wreaks havoc by causing undesirable changes.

## Save Time Generating YAML

References:

- https://kubernetes.io/docs/reference/kubectl/conventions
- https://www.altoros.com/visuals/kubernetes-kubectl-cli-cheat-sheet

`kubectl run` can often save you from needing to create YAML from scratch.

```bash
# create nginx pod
kubectl run --generator=run-pod/v1 nginx --image=nginx

# generate pod manifest yaml
kubectl run --generator=run-pod/v1 nginx --image=nginx --dry-run -o yaml

# create deployment
kubectl run --generator=deployment/v1beta1 nginx --image=nginx

# generate deployment manifest yaml
kubectl run --generator=deployment/v1beta1 nginx --image=nginx --dry-run -o yaml

# generate deployment yaml with 4 replicas
kubectl run --generator=deployment/v1beta1 nginx --image=nginx --dry-run --replicas=4 -o yaml

# save to file
kubectl run --generator=deployment/v1beta1 nginx --image=nginx --dry-run --replicas=4 -o yaml > nginx-deployment.yaml
```

## Pods

Reference: https://kubernetes.io/docs/concepts/workloads/pods/pod-overview

Simplest object in Kubernetes, the foundational building block housing your containers.
Alomst everything is a pod!

```bash
kubectl create -f pod.yml
kubectl get pods
kubectl describe pod mypod
kubectl delete pod mypod
```

## ReplicaSets

Reference: https://kubernetes.io/docs/concepts/workloads/controllers/replicaset

As the name implies, a `ReplicaSet`'s job is replicating pods and maintaining a stable set
of replicas over time. It's how you guarnatee a specific number of identical pods are running.

```bash
kubectl create -f replicaset.yml
kubectl get replicaset
kubectl delete replicaset myapp-replicaset

# e.g. update replicas to scale, good gitops
kubectl replace -f replicaset.yml

# this would scale but not keep yaml definition up to date
kubectl scale -replicas=6 -f replicaset.yml
kubectl scale -replicas=6 replicaset myapp-replicaset
```

## Deployments

Reference: https://kubernetes.io/docs/concepts/workloads/controllers/deployment

tl;dr Deployments create ReplicaSets which create Pods.

Deployments provide delcarative updates for Pods and ReplicaSets. The Deployment Controller
constantly compares _desired state_ and _current state_, resolving differences at a
controlled rate.

```bash
kubectl create -f deployment.yml
kubectl get deployments
# see all created objects
kubectl get all
```

## Namespaces

Reference: https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces

Namespaces provide workload isolation. Each namespace can contain their own resources
and policies. Within a namespace, services can be accessed via short-name (e.g. web,
db, etc.). Services can also talk across namespaces, but must use fully qualified
names in the form service-name.namespace-name.svc.cluster.local (cluster.local is the
cluster's default domain).

By default, kubectl works on the default namespace. To target another namespace,
use the --namespace argument.

```bash
kubectl get pods --namespace=kube-system
kubectl create -f pod.yml --namespace=dev
```

The namespace can also be specified in yaml metadata:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  namespace: dev
```

Namespaces can be created like any other object:

```bash
cat >namespace.yml <<EOF
apiVersion: v1
kind: Namespace
metadata:
  name: dev
EOF

kubectl create -f namespace.yml
```

Or create directly:

```bash
kubectl create namespace dev
kubectl get namespaces
kubectl describe namespace dev

# make dev the default target
kubectl config set-context $(kubectl config current-context) --namespace=dev
# now targets dev vs default namespace
kubectl get pod

# see pods in all namespaces
kubectl get pods --all-namespaces
```

Namespaces can utilize quotas:

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: compute-quota
  namespace: dev
spec:
  hard:
    pods: "10"
    requests.cpu: "4"
    requests.memory: 5Gi
    limits.cpu: "10"
    limits.memory: 10Gi
```

## Services

Reference: https://kubernetes.io/docs/concepts/services-networking/service

Services enable loose coupling between microservices, facilitating communication between groups of pods and external resources.

Types of services:

- `NodePort`: Proxies a port in range 30000-32767 on every node to pod:port
- `ClusterIP`: provides virtual cluster IP mapped to internal services (default)
- `LoadBalancer`: Provisions IaaS load balancer for your service

```bash
kubectl create -f service.yml
kubectl get services
```

Services can span nodes. Target pods are chosen via selector
criteria (e.g. labels). Distribution algorithm is random with session affinity.

## Labels and Selectors

Reference: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels

How does a deployment or service know which pods to target? Labels! Labels are simple
key/value pairs attached to objects, and selectors allow discovery based on labels.

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: myapp-replicaset
  labels: # key/value pairs of the ReplicaSet itself
    app: myapp
    type: front-end
spec:
  template:
    metadata:
      name: myapp-pod
      labels: # key/value pairs of created pods
        app: myapp
        type: front-end
    spec:
      containers:
        - name: nginx-controller
          image: nginx
replicas: 3
selector: # how the ReplicaSet knows which pods to manage
  matchLabels:
    type: front-end
```

Working with labels from CLI:

```bash
kubectl get pods --show-labels
kubectl label nodes node1 size=large
kubectl label pods mypod tier=frontend
kubectl get pods -l tier=frontend,app=myapp,bu=finance
```

## Taints and Tolerations

Reference: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration

Taints are applied to nodes, tolerations to pods. By default, master nodes are tainted
since the best practice is keeping workloads off of the control plane.

`taint-effect` can be `NoSchedule`, `PreferNoSchedule` or `NoExecute`.

- `NoSchedule`: Do not schedule new pods here
- `PreferNoSchedule`: Only schedule here as last resort (e.g. no capacity on other nodes)
- `NoExecute`: Don't schedule, and also evict running pods.

```bash
# generic form
kubectl taint nodes node-name key=value:taint-effect

# add a taint
kubectl taint nodes node1 app=blue:NoSchedule
# remove taint
kubectl taint nodes node01 app-
```

## Resource Limits

Reference: https://kubernetes.io/docs/concepts/policy/resource-quotas

**Note when working with Kubernetes resources:**

- K/M/G = Kilobyte/Megabyte/Gigabyte (decimal; e.g. 1000 Kilobytes == 1 Megabyte)
- Ki/Mi/Gi = Kibibyte/Mebibyte/Gibibyte (binary like you probably expect; e.g. 1024 Kibibytes = 1 Mebibyte)

Add a resources section to your pod definition, with `requests` and `limits`:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: myapp
  labels:
    name: myapp
spec:
  containers:
    - name: myapp
      image: myapp
      ports:
        - containerPort: 8080
      resources: # set for each container in a pod
        requests: # raise the defaults
          memory: "1Gi"
          cpu: 1
        limits: # cap usage
          memory: "2Gi"
          cpu: 2
```

## DaemonSet

Reference: https://kubernetes.io/docs/concepts/workloads/controllers/daemonset

Definition looks similar to a `ReplicaSet`, but ensures one pod instance is always running on
each node. Good for monitoring/logging agents, container networking, etc. (e.g. `kube-proxy` is
a `DaemonSet`).

```bash
kubectl get daemonsets
kubectl get daemonsets --all-namespaces
kubectl describe daemonsets your-ds
```

## Schedulers

References:

- https://kubernetes.io/docs/concepts/scheduling/kube-scheduler
- https://kubernetes.io/docs/tasks/administer-cluster/configure-multiple-schedulers

The `kube-scheduler` is just another pod. By default it is ran with the name `default-scheduler`.
You can create your own by running additional kube-schedulers with `--scheduler-name=your-scheduler`.
Clusters can run many schedulers at once. In a small cluster with one master, simply set
`--leader-elect=false`. For multi-master clusters, `--leader-elect=true` is required, and you should
also pass in `--lock-object-name=your-scheduler`.

```bash
kubectl create -f scheduler-pod-def.yml
kubectl get schedulers
kubectl get pods --namespace=kube-system
```

Instruct pods to use a specific scheduler:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
    - image: nginx
      name: nginx
  schedulerName: your-scheduler
```

```bash
# see which scheduler is used
kubectl get events --namespace=kube-system
kubectl logs your-scheduler --namespace=kube-system
```

## Monitoring and Logging

Reference: https://kubernetes.io/docs/tasks/debug-application-cluster

Heapster has been deprecated, but lives on in minimal form as `metrics-server`. `metrics-server`
is an in-memory store, with introspection provided by kubelet/cAdvisor. This means you will
need third-party tools (Prometheus, ELK, etc) for history/trending.

```bash
# enable in minikube
minikube addons enable metrics-server

# real world - cone and deploy
git clone https://github.com/kubernetes-incubator/metrics-server.git
kubectl create -f deploy/1.8+/

# get node cpu and memory consumption
kubectl top node

# pod metrics
kubectl top pod
```

`kubectl logs` is similar to `docker logs`...

```bash
# typical case
kubectl logs -f pod-name
# if your pod houses multiple containers (e.g. sidecars)
kubectl logs -f pod-name container-name
```

Also useful for troubleshooting:

```bash
kubectl events get events --namespace=your-namespace
```

## Lifecycle Management

```bash
kubectl rollout status deployment/myapp-deployment
kubectl rollout history deployment/myapp-deployment
```

Rollout strategies:

- Recreate (red/green pattern): Delete all existing, deploy new version... includes downtime.
- Rolling Update (blue/green pattern, the default): Roll pods one by one, no downtime.

Simply bump image version in deployment.yml and `kubectl apply -f deployment.yml`.

`kubectl describe deployment myapp-deployment`'s `StrategyType` field will let you know
what strategy is being used. You can also monitor rollouts with `kubectl get replicasets`,
you will see the old and new deployment state changes.

If you spot an issue, and need to rollback:

```bash
kubectl rollout undo deployment/myapp-deployment
# confirm
kubectl get replicasets
```

## ConfigMaps

Pods can specify `env:` in their container definitions, but to separate "config from code"
(config from config? regardless, it's cleaner and enables easier reuse), you want ConfigMaps.
You can create these via `kubectl create configmap`, but stick to declaring in git-backed
YAML like anything else:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: app-config
data:
  APP_COLOR: blue
  APP_MODE: prod
```

```bash
kubectl create -f myapp-configmap.yml
kubectl get configmaps
kubectl describe configmaps
```

For pods to consume a ConfigMap, you need an `envFrom` section in your container spec:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: webapp
  labels:
    name: webapp
spec:
  containers:
    - name: webapp
      image: webapp
      ports:
        - containerPort: 8080
      envFrom:
        - configMapRef:
          name: app-config
```

Only want a specific value vs the entire config?

```yaml
env:
  - name: APP_COLOR
    valueFrom:
      configMapKeyRef:
        name: app-config
        key: APP_COLOR
```

## Secrets

Hey more YAML!

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: webapp-secret
data:
  DB_HOST: mysql
  DB_USER: root
  DB_PASSWORD: password # plaintext!
```

Then just `kubectl create -f secret.yml` ... but that's a secret in plaintext just waiting
to be accidentally comitted to git!

A common solution is "just base64 encode it" -- OK:

```bash
# use this value in secret.yml
echo -n 'password' | base64
```

But base64 IS NOT A HASH! It can be easily reversed:

```bash
# grumpy cat says NOPE.
echo -n 'valueFromAbove' | base64 --decode
```

So even if you base64 encode the values, DO NOT COMMIT THIS TO GIT. Or,
use some sort of manifest-generating process where values are pulled from
your credential manager of choice and then encoded and used to create
the manifest. Grumpy cat thanks you.

Once you've created a secret, you can reference it in a pod:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: webapp
  labels:
    name: webapp
spec:
  containers:
    - name: webapp
      image: webapp-image
      ports:
        - containerPort: 8080
      envFrom:
        - secretRef:
            name: webapp-secret
```

## Draining Nodes

To safely evict pods for node maintenance:

```bash
kubectl drain node01
```

This gracefully terminates all pods on node01, reschedules them on other nodes, and cordons
node01 so new pods can not be scheduled. When maintenance is done:

```bash
kubectl uncordon node01
```

You can also cordon yourself:

```bash
kubectl cordon node02
```

This does not move pods, it just keeps new pods from being scheduled.
